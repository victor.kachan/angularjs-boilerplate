'use strict';

define(function(require, exports, module) {

    require('angular');
    exports.ng = window.angular;

    exports.createModule = require('./scripts/create-module');

    exports.bootstrap = function(el, deps) {
        return exports.ng.bootstrap(el, deps);
    };

    exports.inject = function(moduleName) {
        var deps = ['ng'];
        deps.push(moduleName);
        return exports.ng.injector(deps);
    };
});
