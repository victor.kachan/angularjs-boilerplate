'use strict';

initUnitTests();

var llaBase = require('../../../main');

var module = llaBase.createModule('module', []);

describe('Module', function() {

    beforeEach(ngModule('app'));

    it('should be an object', function() {
        expect(module).to.be.a('object');
    });

    it('bootstrap', function() {
        var bootstrap = llaBase.bootstrap(document, ['module'], {});
        expect(bootstrap).to.be.a('object');
    });

    it('injector', function() {
        var $injector = llaBase.inject(module.name);
        expect($injector.get('$injector')).to.equal($injector);
        expect($injector.invoke(function($injector) {
            return $injector;
        })).to.equal($injector);
    });
});
