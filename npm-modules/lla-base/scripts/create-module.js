'use strict';

define(function(require, exports, module) {

    require('angular');
    var angular = window.angular;

    module.exports = function(name, deps, fn) {

        var ngModule;

        ngModule = angular.module(name, deps, fn);

        return ngModule;
    };
});
