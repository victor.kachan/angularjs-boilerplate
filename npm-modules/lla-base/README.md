# lla-base
Base library for creating angular modules and bootstrapping the app.

## Dependencies

- angular 1.6.2

## Dev Dependencies

- lla-ut-runner ^1.0.0

## Install
```bash
npm i lla-base
npm i lla-base --save
```
## Test
```bash
npm run test
npm run cover
```
## Develop
```bash

```
## Usage

- Create Angular Module

```javascript

module.name = 'module-demo';

var llaBase = require('lla-base');
var deps = []; // no deps

// Create Angular Module
module.exports = llaBase.createModule(module.name, deps);
```
- Start Angular Application

```javascript
var llaBase = require('lla-base');

llaBase.bootstrap(document, ['app'], {
    strictDi: true
});
```