# lla-theme
Base LLA sass/less theme includes basic variables, iconography and typography.

## Dependencies

- bootstrap-less ^3.3.8",
- bootstrap-sass ^3.3.7",
- node-less ^1.0.0",
- node-sass ^3.11.2"
- bootstrap-select 1.12.1

## Dev Dependencies

NO DEPS

## Install
```bash
npm i lla-theme
npm i lla-theme --save
```
## Test
```bash
NO TESTS
```
## Develop
```bash
```
## Usage

- Include into project's sass

```scss
@import "~lla-theme/sass/main";
```