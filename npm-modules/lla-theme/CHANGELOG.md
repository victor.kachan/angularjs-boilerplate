### v 1.0.0
* Initial release

### v 1.0.1
* Added .npmignore file for publishing

### v 1.0.2
* Added bootstrap-select dependency

### v 1.0.3
* Stick v1.12.1 of bootstrap-select dependency

### v 1.0.4
* Sass upgraded to ^4.0.0

### v 1.0.5
* Enable fglyphicons

### v 1.1.0
* Add loading placeholders style

### v 1.1.1
* Add mercury to variables

### v 1.1.2
* Add new colors to variables

### v 1.1.3
* Add new colors to variables

### v 1.1.4
* Add new colors to variables

### v 2.0.0
* Delete less package from module

### v 2.0.1
* Remove $alabaster color as duplicate

### v 2.1.0
* Add $cornflower-blue

### v 2.2.0
* Add $nevada

### v 2.3.0
* Add $wild-sand