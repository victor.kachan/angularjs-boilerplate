# AngularJS NPM Modules
    
### About NPM Modules

AngularJs NPM modules can be used for the angular project as small bits of reusable self sufficient logical building blocks. Just inject any of them as a dependency of your project, import into the code and use.

More about NPM read here [https://docs.npmjs.com/getting-started/what-is-npm](https://docs.npmjs.com/getting-started/what-is-npm)

## Modules list

* lla-base - angular module wrapper
* lla-theme - base theme, references bootstrap [http://getbootstrap.com/docs/3.3/](http://getbootstrap.com/docs/3.3/)
* lla-ut-helpers - helpers for unit testing
* lla-ut-runner - unit test runner
* lla-utils - useful shared utilities 