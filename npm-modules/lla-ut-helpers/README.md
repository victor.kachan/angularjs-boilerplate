# lla-ut-helpers
LLA Unit Tests helper functions. Included following functions:
- getPromise
- testAsync
- testAsyncErrorThrown

## Dependencies

NO DEPS

## Dev Dependencies

NO DEPS

## Install
```bash
npm i lla-ut-helpers
npm i lla-ut-helpers --save
npm i lla-ut-helpers --save-dev
```
## Test
```bash
NO TESTS
```
## Develop
```bash

```
## Usage

- In the unit test
```javascript
var helpers = require('lla-ut-helpers');

angular.module('app', [])
    .service('SomeService', function () {
        return {
            doReject: false,
            getSomething: function () {
                var data = {};
                return helpers.getPromise(data, this.doReject);
            }
        }
    });
```
