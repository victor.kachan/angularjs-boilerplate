'use strict';

module.exports = {
    getPromise: function(data, doReject, errorData) {
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                doReject ? reject(errorData || false) : resolve(data || true);
            }, 200);
        });
    },

    testAsync: function(done, promise, testFunction) {
        promise.then(
            function (data) {
                try {
                    testFunction(data);
                    done()
                } catch (err) {
                    done(err);
                }
            },
            done);
    },

    testAsyncRejection: function(done, promise, testFunction) {
        promise.then(
            done,
            function (error) {
                try {
                    testFunction(error);
                    done()
                } catch (err) {
                    done(err);
                }
            });
    },

    testAsyncErrorThrown: function(done, promise, testFunction) {
        promise.catch(function(error) {
            testFunction(error);
            done();
        });
    },

    getAngularFormMock: function(fieldNames) {
        var formMock = {
            $valid: undefined,
            checkValidity: function() {
                var formValid = true;

                for(var i = 0; i < fieldNames.length; i++) {
                    var fieldError = formMock[fieldNames[i]].$error;
                    var fieldErrorKeys = Object.keys(fieldError);

                    for (var j = 0; j < fieldErrorKeys.length; j++) {
                        if(fieldError[fieldErrorKeys[j]] === false) {
                            formValid = false;
                            break;
                        }
                    }

                    if(!formValid) {
                        break;
                    }
                }

                this.$valid = formValid;
            }
        };

        for(var i = 0; i < fieldNames.length; i++) {
            formMock[fieldNames[i]] = {
                $error: {},
                $setValidity: function (validationErrorKey, value) {
                    this.$error[validationErrorKey] = value;
                    formMock.checkValidity();
                }
            }
        }

        return formMock;
    }
};
