var path = require('path');
var nodeExternals = require('webpack-node-externals');
var isCoverage = process.env.NODE_ENV === 'coverage';

module.exports = {
    context: path.resolve('./'),
    output: {
        devtoolModuleFilenameTemplate        : '[absolute-resource-path]',
        devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
    },
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
    module: {
        rules: [
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.scss$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.(jpg|jpeg|gif|png|svg|woff|woff2|ttf|eot|ico)(\?\S*)?$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.zip$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.scss$/,
                loader: 'ignore-loader',
                exclude: [/main\.scss/]
            }
        ].concat(
            isCoverage ? {
                    test: /\.js/,
                    include: path.resolve('./scripts'),
                    exclude: /(tests|node_modules|\.spec\.js|main\.js$|\.json$)/,
                    loader: 'istanbul-instrumenter-loader'
                }: [])
    },
    devtool: '#inline-cheap-module-source-map',
    resolve: {
        modules: [
            __dirname,
            'node_modules'
        ],
        extensions: ['.js', '.json']
    }
};
