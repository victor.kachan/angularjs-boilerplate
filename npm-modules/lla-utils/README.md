# lla-utils
Utilities module.

## Dependencies

- lla-base ^1.0.0
- angular 1.6.2
- ngstorage 0.3.11

## Dev Dependencies

- lla-ut-runner ^1.0.0

## Install
```bash
npm i lla-utils
npm i lla-utils --save
```
## Test
```bash
npm run test
npm run cover
```
## Develop
```bash
```
## Usage
```javascript
var llaUtils = require('lla-utils');
//add to the module as dependency
llaBase.createModule('app', [llaUtils])
    .controller('AppController',
        function (SomeService) {
            SomeService.init();
        }
    );
```