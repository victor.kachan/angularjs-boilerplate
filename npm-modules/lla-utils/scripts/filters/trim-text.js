'use strict';

define(function (require, exports, module) {

    /**
     * @ngInject
     * @constructor
     *
     * @ngdoc filter
     * @module lla-utils
     * @name trimText
     *
     * @description
     *
     **/
    exports.trimText = function () {
        return function (s, len) {
            if (typeof s !== 'string') {
                s = '';
            }

            if (!s || s.length <= len) {
                return s;
            } else {
                var maxLength = len || 100,
                    ellipsis = '...';

                //trim the string to the maximum length
                var trimmedString = s.substr(0, maxLength);

                //re-trim if we are in the middle of a word
                trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')))
                    + ((s.length > maxLength) ? ellipsis : '');

                return trimmedString;
            }
        }
    };
});
