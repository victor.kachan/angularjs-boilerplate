'use strict';

define(function (require, exports) {

    /**
     * @ngInject
     * @constructor
     *
     * @ngdoc filter
     * @module lla-utils
     * @name capitalize
     *
     * @description
     *
     **/
    exports.capitalize = function () {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    };
});
