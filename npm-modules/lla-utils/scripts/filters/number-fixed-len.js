'use strict';

define(function (require, exports, module) {

    /**
     * @ngInject
     * @constructor
     *
     * @ngdoc filter
     * @module lla-utils
     * @name numberFixedLen
     *
     * @description
     *
     **/
    exports.numberFixedLen = function () {
        return function (n, len) {
            return typeof n === 'number' ? //0 is also number and +n approach doesn't work here
                (Array(len).join('0') + n.toString()).slice(-len) :
                (console.error('filter.js NumberFixedLen: Incoming parameter must be a number') || null);
        }
    };
});
