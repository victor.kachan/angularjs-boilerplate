'use strict';

initUnitTests();

var llaUtils = require('../../../main');

angular.module('app', [llaUtils]);

describe('Utils Module', function() {

    beforeEach(ngModule('module-utils'));

    it('should return an object', function() {
        expect(llaUtils).to.be.eql('lla-utils');
    });
});
