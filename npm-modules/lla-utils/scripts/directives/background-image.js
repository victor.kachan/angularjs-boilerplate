'use strict';

define(function (require, exports, module) {

    /**
     * @ngInject
     * @constructor
     *
     * @ngdoc directive
     * @module lla-utils
     * @name backImg
     *
     * @description
     *
     **/
    exports.backImg = function () {
        return function (scope, element, attrs) {
            var stopObserving;

            function setBackground() {
                var url = attrs.backImg;

                if (url) {
                    element.css({
                        'background-image': 'url(' + url + ')'
                    });

                    stopObserving();
                }
            }

            stopObserving = attrs.$observe('backImg', setBackground)
        };
    };
});
