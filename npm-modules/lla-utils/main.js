'use strict';

define( function (require, exports, module) {

    module.name = 'lla-utils';

    var llaBase = require('lla-base');

    var deps = [
    ];

    /**
     * @ngInject
     *
     * @ngdoc module
     * @name lla-utils
     *
     *
     * @description
     *
     * TODO + COMPONENTS
     */
    module.exports = llaBase.createModule(module.name, deps)
        .directive(require('./scripts/directives/background-image'))
        .filter(require('./scripts/filters/number-fixed-len'))
        .filter(require('./scripts/filters/trim-text'))
        .filter(require('./scripts/filters/capitalize'))
        .name;
});
