# lla-ut-runner
LLA Unit Tests runner. Simulates synthetic testing environment and includes following libraries: 
- mocha - unit test framework / https://mochajs.org/
- istanbul - for generating coverage reports / https://istanbul.js.org/
- chai - BDD / TDD assertion library / http://chaijs.com/
- sinon - mock library for js unit tests / http://sinonjs.org/

## Dependencies

- angular 1.6.2

## Dev Dependencies

- amd-loader 0.0.5
- angular 1.6.2
- angular-mocks 1.6.2
- chai ^3.5.0
- jsdom ^9.12.0
- mocha ^3.2.0
- mocha-webpack ^0.7.0
- sinon ^2.0.0
- nyc ^10.1.2

## Install
```bash
npm i lla-ut-runner
npm i lla-ut-runner --save
npm i lla-ut-runner --save-dev
```
## Test
```bash
NO TESTS
```
## Develop
```bash

```
## Usage

- Requirement for mocha command line
```bash
mocha --require lla-ut-runner './**/*.spec.js'
```
- In the unit test
```javascript
//initialize synthetic unit testing environment
initUnitTests();

describe('AuctionDetails component', function () {
    /**
    * then to register a module
    * read more about angular.mock.module here
    * https://docs.angularjs.org/api/ngMock/function/angular.mock.module
    */
    beforeEach(ngModule('app'));
    
    /**
    * then to inject into a module
    * read more about angular.mock.module here
    * https://docs.angularjs.org/api/ngMock/function/angular.mock.inject
    */
    beforeEach(ngInject(function (_$componentController_) {
        $componentController = _$componentController_;
    }));
});
```
