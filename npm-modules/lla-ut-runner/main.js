'use strict';

var Mocha = require('mocha');
var chai = require('chai');
var sinon = require('sinon');
var jsdom = require('jsdom').jsdom;
var mocha = new Mocha();
require('amd-loader');

global.initUnitTests = function() {

    delete require.cache[require.resolve('angular')];
    delete require.cache[require.resolve('angular/angular')];
    delete require.cache[require.resolve('angular-mocks')];
    delete require.cache[require.resolve('angular-mocks/angular-mocks')];

    global.document = jsdom('');
    global.window = global.document.defaultView;
    global.navigator = window.navigator = {};
    global.Node = window.Node;
    global.sinon = sinon;
    global.expect = require('chai').expect;

    require('angular/angular');
    global.angular = window.angular;

    // emulate mocha running in browser (makes angular-mocks work)
    window.mocha = true;
    window.beforeEach = beforeEach;
    window.afterEach = afterEach;
    require('angular-mocks');

    //Helpers
    global.ngModule = global.angular.mock.module;
    global.ngInject = global.angular.mock.inject;
};

module.exports = global.initUnitTests;
