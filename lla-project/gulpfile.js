var gulp = require('gulp');
var path = require('path');
var webpack = require('webpack');
var browserSync = require('browser-sync');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
//var modRewrite  = require('connect-modrewrite');
var webpackConfig = require('./conf/webpack.config.test.js');

var bundler = webpack(webpackConfig);

gulp.task('default', function() {

});

gulp.task('dev', function() {
    browserSync({
        port: 9000,
        open: false,
        notify: false,
        ghostMode: false,
        codeSync: false,
        server: {
            baseDir: [ './app/', './.nyc_output/' ],
            middleware: [
                webpackDevMiddleware(bundler, {
                    publicPath: webpackConfig.output.publicPath,
                    stats: {
                        colors: true
                    },
                    noInfo: true
                }),
                webpackHotMiddleware(bundler)
                //,
                // modRewrite([
                //     '!\\.\\w+$ /index.html [L]'
                // ])
            ]
        },
        files: [
            path.join(process.cwd(), '*.ico'),
            path.join(process.cwd(), 'app/**/*.js'),
            path.join(process.cwd(), 'app/**/*.css'),
            path.join(process.cwd(), 'app/**/*.html')
        ],
        socket: {
            socketIoOptions: {
                log: false
            },
            socketIoClientConfig: {
                reconnectionAttempts: 50
            },
            path: '/browser-sync/socket.io',
            clientPath: '/browser-sync',
            namespace: '/browser-sync',
            clients: {
                heartbeatTimeout: 5000
            }
        },
    });
})