'use strict';

define(function (require, exports, module) {

    /**
     * Asynchronous loading of the module and initialization
     * @param modulePath - path to the module relative to the app folder
     * @returns {{lazyLoad: *[]}} - resolver for the state.resolve
     */
    module.exports = function (modulePath) {
        var resolver = {
            'lazyLoad': ['$q', '$ocLazyLoad', function ($q, $ocLazyLoad) {
                var defer = $q.defer();
                require.ensure([], function (require) {
                    var waitForModule = require('bundle-loader?name=[path][name]!../../../../app/' + modulePath);
                    waitForModule(function (module) {
                        $ocLazyLoad.load({
                            name: module.name
                        });
                        defer.resolve(module);
                    });
                });
                return defer.promise;
            }]
        }
        return resolver;
    };
});
