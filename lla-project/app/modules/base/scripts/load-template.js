'use strict';

define(function (require, exports, module) {

    /**
     * Asynchronous loading of the template
     * @param templatePath - path to the template relative to the app folder
     * @returns {*[]}
     */
    module.exports = function (templatePath) {
        return ['$q', function ($q) {
            var defer = $q.defer();
            require.ensure([], function (require) {
                var waitForTemplate = require('bundle-loader?name=[path][name]!../../../../app/' + templatePath);
                waitForTemplate(function (template) {
                    defer.resolve(template);
                    window.scrollTo(0, 0);
                });
            });
            return defer.promise;
        }];
    };
});
