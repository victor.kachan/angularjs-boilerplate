'use strict';

define(function (require, exports) {

    exports.loader = function () {
        return {
            loadModule: require('./scripts/load-module'),
            loadTemplate: require('./scripts/load-template')
        }
    };
});
