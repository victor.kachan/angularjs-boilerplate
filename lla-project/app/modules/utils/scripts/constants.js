'use strict';

define(function (require, exports) {

    exports.Constants = {
        CONFIG: require('lla-config'),
        SERVER_ERROR: 'Server error!'
    };
});
