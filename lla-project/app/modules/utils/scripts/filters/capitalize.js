'use strict';

define(function (require, exports) {

    /**
     * @ngInject
     */
    exports.capitalize = function () {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    };
});
