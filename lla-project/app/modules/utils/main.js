'use strict';

define(function (require, exports, module) {

    module.name = 'module-utils';

    var llaBase = require('lla-base');

    var deps = [
    ];

    module.exports = llaBase.createModule(module.name, deps)
        .value(require('./scripts/constants'))
        .filter(require('./scripts/filters/number-fixed-len'))
        .filter(require('./scripts/filters/capitalize'))
        .name;
});
