'use strict';

define(function (require, exports) {

    /**
     * @ngInject
     */
    exports.EndPoints = {

        /* Content */
        CONTENT_ITEMS_STATIC:                   '/api/rest/static',
        CONTENT_ITEMS_VARIOUS:                  '/api/rest/various',
        CONTENT_ITEMS_NEWS:                     '/api/rest/news/:id'
    };
});