'use strict';

define(function (require, exports, module) {

    /**
     * Register service
     * @ngInject
     * @constructor
     */
    function ContentService($resource, EndPoints) {

        var DEFAULT_LANG = 'nl';

        return {
            getStaticContentItem: function (title) {
                return $resource(EndPoints.CONTENT_ITEMS_STATIC).get({
                    'title': title,
                    'language': DEFAULT_LANG
                }).$promise;
            },

            getVariousContentItem: function (title) {
                return $resource(EndPoints.CONTENT_ITEMS_VARIOUS).get({
                    'title': title
                }).$promise;
            },

            getNewsContentItem: function (id) {
                return $resource(EndPoints.CONTENT_ITEMS_NEWS).get({
                    'id': id,
                    'language': DEFAULT_LANG
                }).$promise;
            }
        };
    }

    module.exports = ContentService;
});