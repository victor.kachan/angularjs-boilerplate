'use strict';

define(function (require, exports, module) {

    module.name = 'module-api-services';

    var llaBase = require('lla-base'),
        utils = require('utils'),
        ngResource = require('angular-resource'),
        config = require('lla-config');

    var deps = [
        ngResource,
        utils
    ];

    module.exports = llaBase.createModule(module.name, deps)
        .value(require('./endpoints'))
        .config(
            /* @ngInject */
            function ($provide) {
                var apiOrigin = config.API_ORIGIN;
                $provide.decorator('EndPoints', function ($delegate) {
                    if (typeof $delegate === 'object') {
                        for (var e in $delegate) {
                            $delegate[e] = apiOrigin + $delegate[e];
                        }
                    }
                    return $delegate;
                });
            })
        .config(
            /* @ngInject */
            function ($httpProvider) {
                $httpProvider.defaults.cache = false;
            })
        .config(
            /* @ngInject */
            function ($httpProvider, $resourceProvider) {
                $resourceProvider.defaults.actions.get = {
                    method: 'GET',
                    transformResponse: function (data, headers) {
                        var response = JSON.parse(data);
                        response.headers = headers();
                        return response;
                    }
                };
            })
        .service('ContentService', require('./scripts/content'))
        .service('CountriesService', require('./scripts/countries'))
        .name;
});
