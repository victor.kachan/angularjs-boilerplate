'use strict';

define(function () {

    return {
        /* @ngInject */
        init: function ($urlRouterProvider, $stateProvider) {

            /**
             * Wrapper method for registering states for the ui-router
             * @param state - The name of the state for ui-router
             * @param url - url parameter for the state provided
             * @param templatePath - path to the template which needs to be loaded for the state relative to the app folder
             * @param modulePath - path to the module which needs to be initialized for the state relative to the app folder
             */
            function registerRoute(state, url, templatePath, modulePath, params) {

                var base = require('base');

                $stateProvider.state(state, {
                    url: url,
                    templateProvider: base
                        .loader()
                        .loadTemplate(templatePath),
                    resolve: base
                        .loader()
                        .loadModule(modulePath),
                    params: params
                });
            };

            //$locationProvider.hashPrefix('');

            /**
             * Html5mode config to get rid of # in the url
             * Modrewrite on the server needs to be enabled
             * RewriteEngine On
             * Options FollowSymLinks
             *
             * RewriteBase /
             *
             * RewriteCond %{REQUEST_FILENAME} !-f
             * RewriteCond %{REQUEST_FILENAME} !-d
             * RewriteRule ^(.*)$ /#/$1 [L]
             */
            // $locationProvider.html5Mode({
            //     enabled: true,
            //     requireBase: false
            // });

            /**
             * Reading routes from json and configuring routes for SPA
             */
            var routes = require('./routes.json');
            for (var route in routes) {
                registerRoute(
                    route,
                    routes[route].url,
                    routes[route].template,
                    routes[route].module,
                    routes[route].params
                );
            }

            $urlRouterProvider.otherwise('/');
        }
    }
});
