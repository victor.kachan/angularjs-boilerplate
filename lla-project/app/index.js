'use strict';

require(['lla-base'], function (llaBase, require) {

    module.name = 'angularjs.site.app';

    var angularUiRouter = require('angular-ui-router'),
        angularTranslate = require('angular-translate'),
        angularTranslateLoaderPartial = require('angular-translate-loader-partial'),
        oclazyload = require('oclazyload'),
        uiConponents = require('ui-components'),
        llaUtils = require('lla-utils');

    window.jQuery = require('jquery');
    require('bootstrap-sass');

    var deps = [
        angularUiRouter,
        angularTranslate,
        angularTranslateLoaderPartial,
        oclazyload,
        uiConponents,
        llaUtils
    ];

    if (DEV) {
        //add mock module here
    }

    function run() {
    }

    var app = llaBase.createModule(module.name, deps)
        .controller('AppController',
            /* @ngInject */
            function () {
            })
        .config(require('./router-config').init)
        .config(
            /* @ngInject */
            function ($translateProvider) {
                $translateProvider
                    .preferredLanguage('nl')
                    .fallbackLanguage('nl')
                    .useSanitizeValueStrategy('sanitize')
                    .translations('nl', require('./locale/nl.json'))
                    .translations('en', require('./locale/en.json'))
                    .translations('de', require('./locale/de.json'));
            })
        .run(run);

    llaBase.bootstrap(document, [app.name], {
        strictDi: true
    });
});
