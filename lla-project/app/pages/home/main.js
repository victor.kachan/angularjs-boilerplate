'use strict';

define(function (require, exports, module) {

    module.name = 'index-page';

    var llaBase = require('lla-base'),
        uiConponents = require('ui-components');

    var deps = [
        uiConponents
    ];

    module.exports = llaBase.createModule(module.name, deps)
        .controller('IndexController', require('./scripts/controller'));
});
