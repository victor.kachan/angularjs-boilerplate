'use strict';

define(function (require, exports, module) {

    /**
     * Index controller
     * @ngInject
     * @constructor
     */
    function FooterController() {
        var ctrl = this;

        ctrl.$onInit = function () {
        }
    }

    module.exports = FooterController;
});
