'use strict';

define(function (require, exports, module) {

    /**
     * @ngInject
     */
    module.exports = {
        bindings: {},
        template: require('./template.html'),
        controller: require('./scripts/controller.js')
    }
});
