'use strict';

define(function (require, exports, module) {

    /**
     * Index controller
     * @ngInject
     * @constructor
     */
    function HeaderController() {
        var ctrl = this;

        ctrl.$onInit = function () {
        }
    }

    module.exports = HeaderController;
});
