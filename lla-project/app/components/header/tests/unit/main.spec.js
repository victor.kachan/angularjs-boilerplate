'use strict';

initUnitTests();

angular.module('app', [])
    .component('myHeader', require('../../main'));

describe('Header component', function() {

    var $componentController;

    beforeEach(ngModule('app'));

    beforeEach(ngInject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should be an object', function() {
        var bindings = {};
        var ctrl = $componentController('myHeader', null, bindings);
        ctrl.$onInit();
        expect(ctrl).to.be.a('object');
    });
});