'use strict';

initUnitTests();

angular.module('app', [])
    .component('myLangSwitcher', require('../../main'));

describe('LangSwitcher component', function() {

    var $componentController;

    beforeEach(ngModule('app'));

    beforeEach(ngInject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    it('should be an object', function() {
        var bindings = {};
        var ctrl = $componentController('myLangSwitcher', null, bindings);
        ctrl.$onInit();
        expect(ctrl).to.be.a('object');
    });
});