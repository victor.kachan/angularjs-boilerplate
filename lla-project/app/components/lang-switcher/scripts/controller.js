'use strict';

define(function (require, exports, module) {

    /**
     * Index controller
     * @ngInject
     * @constructor
     */
    function LangSwitcherController($translate) {
        var ctrl = this;

        ctrl.$onInit = function () {
        };

        ctrl.switch = function(ln) {
            $translate.use(ln);
        }
    }

    module.exports = LangSwitcherController;
});
