'use strict';

define(function (require, exports, module) {

    module.name = 'ui-components';

    var llaBase = require('lla-base'),
        llaUtils = require('lla-utils'),
        api = require('api'),
        utils = require('utils'),
        ngSanitize = require('angular-sanitize'),
        ngMessages = require('angular-messages');

    var deps = [
        api,
        llaUtils,
        utils,
        ngSanitize,
        ngMessages
    ];

    module.exports = llaBase.createModule(module.name, deps)
        .component('myLangSwitcher', require('./lang-switcher/main'))
        .component('myHeader', require('./header/main'))
        .component('myFooter', require('./footer/main'))
        .component('myTodo', require('./todo/main'))
        .name;
});