var path = require('path');
var merge = require('merge-deep');
var webpack = require('webpack');
var config = require('./webpack.config.js');

var extConfig = {
    output: {
        chunkFilename: '[chunkhash].bundle.js'
    },
    resolve: {
        alias: {
            // Will take the npm nodule instead
        }
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            mangle: true,
            compress: {
                sequences       : true,
                booleans        : true,
                loops           : true,
                unused          : true,
                unsafe          : true,
                drop_console    : true,
                drop_debugger   : true,
                keep_fnames     : false
            }
        })
    ]
};

module.exports = merge(config, extConfig);