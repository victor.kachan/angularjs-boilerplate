var path = require('path');
var merge = require('merge-deep');
var config = require('./webpack.config.js');

var extConfig = {
    output: {
        chunkFilename: '[name].bundle.js'
    },
    module: {
        rules: [
            //HTML
            {
                test: /(\.htm|\.html)$/,
                enforce: 'pre',
                loader: 'htmllint-loader?' + JSON.stringify({
                    config: '.htmllintrc',
                    failOnWarning: false,
                    failOnError: true
                }),
                exclude: /node_modules|tests|npm-modules/
            },
            // Javascript
            {
                test: /\.js?$/,
                enforce: 'pre',
                loader: 'eslint-loader?' + JSON.stringify({
                    failOnWarning: false,
                    failOnError: true
                }),
                exclude: /node_modules|tests|npm-modules/
            }
        ]
    },
    devtool: '#eval',
    resolve: {
        alias: {
            'lla-config': path.resolve('./conf/config.acc.json')
        }
    }
};

module.exports = merge(config, extConfig);