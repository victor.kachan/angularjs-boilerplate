var path = require('path');
var merge = require('merge-deep');
var webpack = require('webpack');
var config = require('./webpack.config.js');

var extConfig = {
    output: {
        chunkFilename: '[chunkhash].bundle.js'
    },
    resolve: {
        alias: {
            'lla-config': path.resolve('./conf/config.acc.json')
        }
    },
    devtool: '#source-map',
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            beautify: false,
            comments: false,
            mangle: true,
            compress: {
                sequences       : true,
                booleans        : true,
                loops           : true,
                unused          : true,
                unsafe          : true,
                drop_console    : true,
                drop_debugger   : true,
                keep_fnames     : false
            }
        })
    ]
};

module.exports = merge(config, extConfig);