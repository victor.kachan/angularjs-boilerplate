/**
 * Created by Victor Kachan on 28/02/2017.
 * Common aliases for dev and prod envs
 */
var path = require('path');

module.exports = {
    'base': path.resolve('./app/modules/base/main'),
    'api': path.resolve('./app/modules/services/api/main'),
    'utils': path.resolve('./app/modules/utils/main'),
    'ui-components': path.resolve('./app/components/main')
};