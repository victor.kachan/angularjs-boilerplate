var webpack = require('webpack');
var path = require('path');
var merge = require('merge-deep');
var autoprefixer = require('autoprefixer');
var ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var WebpackMergeJsonsPlugin = require("webpack-merge-jsons-plugin");
var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var DIST_FOLDER = path.resolve('./dist');

module.exports = {
    entry: {
        bundle: ['babel-polyfill', path.resolve('./app/index.js')]
    },
    output: {
        path: DIST_FOLDER,
        publicPath: '/',
        filename: '[hash].js',
        chunkFilename: '[chunkhash].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader'})
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!resolve-url-loader!sass-loader?sourceMap'})
            },
            {
                test: /\.(jpg|jpeg|gif|png|svg|woff|woff2|ttf|eot|ico)(\?\S*)?$/,
                loader: 'file-loader?name=assets/[name].[ext]'
            },
            {
                test: /\.zip$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.spec\.js$/,
                loader: 'ignore-loader'
            },
            {
                test: /locale\/([a-z]{2})\.json$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.scss$/,
                loader: 'ignore-loader',
                exclude: [/main\.scss/]
            }
        ]
    },
    resolve: {
        modules: [
            __dirname,
            'node_modules'
        ],
        descriptionFiles: ['package.json', 'bower.json'],
        alias: merge(
            {'angular': path.resolve('./node_modules/angular')},
            require('./webpack.aliases.js')),
        extensions: ['.js', '.json']
    },
    // postcss: [
    //     autoprefixer({
    //         browsers: ['last 2 version']
    //     })
    // ],
    plugins: [
        new ContextReplacementPlugin(/\/app\/$/, __dirname + '/app/', false),
        new ExtractTextPlugin({ filename: 'css/[name].css', disable: false, allChunks: true }),
        new CopyWebpackPlugin([
            {
                context: './app/',
                from: 'locale/*',
                to: 'i18n/app'
            },
            {
                context: './app/components/**/',
                from: 'locale/*',
                to: 'i18n/components'
            }
        ]),
        new WebpackMergeJsonsPlugin({
            src: './app/components/**/locale/de.json',
            dest: 'i18n/components/locale/de.json'
        }),
        new WebpackMergeJsonsPlugin({
            src: './app/components/**/locale/en.json',
            dest: 'i18n/components/locale/en.json'
        }),
        new WebpackMergeJsonsPlugin({
            src: './app/components/**/locale/nl.json',
            dest: 'i18n/components/locale/nl.json'
        }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        }),
        new ngAnnotatePlugin({
            add: true
        }),
        new webpack.DefinePlugin({
            DEV: JSON.stringify(false)
        }),
        new HtmlWebpackPlugin({
            template: 'app/index.html',
            favicon: 'app/media/favicon.ico'
        })
    ]
};