# AngularJS Boilerplate

## Getting started

### Install Node.js with a version manager

- *nix: [https://github.com/creationix/nvm](https://github.com/creationix/nvm)
- Windows: [https://github.com/marcelklehr/nodist](https://github.com/marcelklehr/nodist)

### Install development dependencies

    $ npm i
    
### Start developing

    $ npm run dev

- builds the project
- starts a lightweight server on port 9000
- start file watchers
    - file watchers cover the `./app` directory;
      if you need to edit the `config` file or vendor/library code,
      stop the process first and run it again when you are done

### Create a build

    $ npm run build

## Project structure

### `/app`

**Do not put files in this directory that you don't want in the build.**
