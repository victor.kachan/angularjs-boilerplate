# AngularJS Boilerplate to kick start project

AngularJS Boilerplate to kick start any new frontend app project with sample API service for communication with the 
backend, SASS, WebPack, private NPM sample modules, Unit Testing, Gulp for development purposes and lightweight fast 
HTTP server for local development. Also DTAP phased approach supported.

## Getting started

### Install Node.js with a version manager

- *nix: [https://github.com/creationix/nvm](https://github.com/creationix/nvm)
- Windows: [https://github.com/marcelklehr/nodist](https://github.com/marcelklehr/nodist)

### Install project dependencies

In lla-project folder run:

    $ npm i
    
### Start developing

In lla-project folder run:

    $ npm run dev

- builds the project
- starts a lightweight server on port 9000
- start file watchers
    - file watchers cover the `./app` directory;
      if you need to edit the `config` file or vendor/library code,
      stop the process first and run it again when you are done
      
### Run unit tests

    $ npm run test
    
or

    $ npm run cover
    
if you'd like to measure coverage

### Create a build

Several environments are supported. If you need to build for test run :

    $ npm run build:test
    
for acceptance: 

    $ npm run build:acc
    
for production: 

    $ npm run build:prod

## Project structure

* `/app` - source code of the project
* `/app/components` - components of the project, building blocks
* `/app/locale` - translation json files for multilanguage support
* `/app/media` - media files, such as logo, favicon etc.
* `/app/modules` - modules of the project such as services, filters directives etc., which can be represented as independent npm modules and be released to the private npm repository as Nexus or so
* `/app/pages` - SPA pages
* `/app/theme` - custom theme of the project on top of npm module with the base theme
* `/conf` - config files for Webpack and configs for different environments
